#include <ncurses.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "define.h"
#include "map.h"
#include "struct.h"

int game_status;  // ゲームの進行状況

SNAKE *snake_body = NULL;  // ヘビの体
int snake_dir;             // ヘビの向き

FEED feed[FEED_MAX];  // エサ

/**
 * ヘビを追加する
 */
SNAKE *snake_enq(SNAKE *_snake, int _x, int _y) {
  SNAKE *snake_new = NULL;

  // 初期化
  snake_new = malloc(sizeof(SNAKE));
  snake_new->x = _x;
  snake_new->y = _y;
  snake_new->next = NULL;

  if (_snake != NULL) {
    // 次のヘビの位置をセットする
    snake_new->next = _snake;
  }

  return snake_new;
}

/**
 * 最後のヘビを削除する
 */
SNAKE *snake_deq(SNAKE *_snake) {
  SNAKE *snake_top = _snake;
  SNAKE *snake_perv = _snake;

  if (_snake == NULL) {
    return NULL;
  }

  while (_snake->next != NULL) {
    snake_perv = _snake;
    _snake = _snake->next;
  }

  free(_snake);
  _snake = NULL;
  snake_perv->next = NULL;

  return snake_top;
}

/**
 * ヘビを全て削除する
 */
void snake_clr(SNAKE *_snake) {
  SNAKE *snake_next = _snake;
  while (_snake != NULL) {
    snake_next = _snake->next;
    free(_snake);
    _snake = NULL;
    _snake = snake_next;
  }
}

/**
 * ヘビとの当たり判定
 */
bool check_snake(SNAKE *_snake, int _x, int _y) {
  if (_snake->next == NULL) {
    return false;
  }

  _snake = _snake->next;

  while (_snake != NULL) {
    if (_snake->x == _x && _snake->y == _y) {
      return true;
    }
    _snake = _snake->next;
  }

  return false;
}

/**
 * ヘビを描画する
 */
void print_snake(SNAKE *_snake) {
  SNAKE *snake_top = _snake;

  if (_snake == NULL) {
    return;
  }

  while (_snake != NULL) {
    mvprintw(_snake->y, _snake->x, "#");
    _snake = _snake->next;
  }

  mvprintw(snake_top->y, snake_top->x, "@");
}

/**
 * エサとの当たり判定
 */
int check_feed(int _x, int _y) {
  for (int i = 0; i < FEED_MAX; i++) {
    if (feed[i].x == _x && feed[i].y == _y) {
      return i;
    }
  }

  return -1;
}

/**
 * エサを追加
 */
void add_feed(int _num) {
  int x = 0;
  int y = 0;

  do {
    x = rand() % (FIELD_WIDHT - 3) + 1;
    y = rand() % (FIELD_HEIGTH - 4) + 2;
  } while (check_feed(x, y) >= 0);

  feed[_num].x = x;
  feed[_num].y = y;
}

/**
 * エサをランダムに作成
 */
void init_feed() {
  for (int i = 0; i < FEED_MAX; i++) {
    add_feed(i);
  }
}

/**
 * エサを描画
 */
void print_feed() {
  for (int i = 0; i < FEED_MAX; i++) {
    mvprintw(feed[i].y, feed[i].x, "$");
  }
}

/**
 * 壁との当たり判定
 */
bool check_wall(int _x, int _y) {
  if (m_MAP[_y - 1][_x] == 1) {
    return true;
  }
  return false;
}

/**
 * フィールドの描画
 */
void print_map() {
  for (int y = 0; y < FIELD_HEIGTH; y++) {
    for (int x = 0; x < FIELD_WIDHT; x++) {
      if (m_MAP[y][x] == 1) {
        mvprintw(y + 1, x, "*");
      } else {
        mvprintw(y + 1, x, " ");
      }
    }
  }
}

/**
 * メイン関数
 */
int main(int argc, char **argv) {
  srand((int)time(0));  //  乱数系列初期化

  int score = 0;     // スコア
  game_status = 0;   // ゲームの状態
  SNAKE snake_head;  // ヘビの頭

  initscr();
  noecho();     // キーが入力されても表示しない
  curs_set(0);  // カーソルを表示しない
  cbreak();     // Enter待ちしない
  resize_term(FIELD_HEIGTH, FIELD_WIDHT);  // ウインドウサイズ
  timeout(0);  // getchで待ちを発生させない

  while (true) {
    int key = getch();
    if (key == 'q') break;
    switch (key) {
      case 'h':
        if (snake_dir != 3) {
          snake_dir = 2;
        }
        break;
      case 'j':
        if (snake_dir != 0) {
          snake_dir = 1;
        }
        break;
      case 'k':
        if (snake_dir != 1) {
          snake_dir = 0;
        }
        break;
      case 'l':
        if (snake_dir != 2) {
          snake_dir = 3;
        }
        break;

      case 's':
        if (game_status == 0 || game_status == 3) {
          game_status = 1;
        }

        break;
    }

    switch (game_status) {
      case 0:
        // スタート画面
        mvprintw(FIELD_HEIGTH / 2, FIELD_WIDHT / 2 - 12 / 2, "Snake Game!");
        mvprintw(FIELD_HEIGTH / 2 + 2, FIELD_WIDHT / 2 - 16 / 2,
                 "Please Key 's'!");
        break;

      case 1:
        // 初期化
        // スコアのクリア
        score = 0;
        // ヘビの初期位置
        snake_dir = 0;
        snake_clr(snake_body);
        snake_body = NULL;
        snake_body = snake_enq(snake_body, FIELD_WIDHT / 2, FIELD_HEIGTH / 2);

        // エサの初期化
        init_feed();

        game_status++;

        break;
      case 2:
        // ゲーム画面
        snake_head.x = snake_body->x;
        snake_head.y = snake_body->y;

        switch (snake_dir) {
          case 0:
            // 上
            snake_head.y--;
            break;

          case 1:
            // 下
            snake_head.y++;
            break;

          case 2:
            // 左
            snake_head.x--;
            break;

          case 3:
            // 右
            snake_head.x++;
            break;
        }

        snake_body = snake_enq(snake_body, snake_head.x, snake_head.y);

        int hit = check_feed(snake_body->x, snake_body->y);
        if (-1 == hit) {
          // エサを食べてない
          snake_body = snake_deq(snake_body);
        } else {
          // エサを食べた
          add_feed(hit);

          score += 100;  // エサの点数
        }

        if (check_snake(snake_body, snake_body->x, snake_body->y) ||
            check_wall(snake_body->x, snake_body->y)) {
          // ゲームオーバー
          game_status = 3;
        }

        score++;  // 時間経過の点数

        erase();  // 画面をクリア

        mvprintw(0, 0, "SCORE:%06d", score);  // スコアの表示

        print_map();  // フィールドの描画

        print_feed();

        print_snake(snake_body);
        break;

      case 3:
        // ゲームオーバー
        mvprintw(0, 0, "SCORE:%06d", score);  // スコアの表示

        print_map();  // フィールドの描画

        print_feed();

        print_snake(snake_body);

        mvprintw(FIELD_HEIGTH / 2, FIELD_WIDHT / 2 - 11 / 2, "Game Over!");

        break;
    }

    usleep(200000);  // 待機（5FPS）
  }

  snake_clr(snake_body);

  endwin();

  return 0;
}
