#ifndef STRUCT_H_
#define STRUCT_H_

typedef struct SNAKE {
    int x;
    int y;
    struct SNAKE* next;
} SNAKE;

typedef struct FEED {
    int x;
    int y;
} FEED;

#endif // STRUCT_H_
